# Capacitron-Pack

Capacitron-Pack builds a website using Webpack for publishing. Subsequently, deploy the web application to Capacitor (on Android by default) or Electron (on PC by default) as desired.

## Initialization

1. Open your preferred terminal for Git and Node commands
2. Clone this repository
3. Run `cd capacitron-pack`
4. Run `npm install`
5. Run `npm update`

## Build for Web

1. Run `npm run build` to deploy published web content to the `dist` folder
2. Run `serve dist`

## Preview in Electron

1. Build you web content so that it is updated
2. Run `npm run start`

## Preview in Android Studio

1. Build your web content so that it is updated
2. Run `npx cap add android` (only do this once for a project)
3. Run `npx cap copy android`
4. Run `npx cap open android`
5. In **Android Studio** click **Debug 'app'**
