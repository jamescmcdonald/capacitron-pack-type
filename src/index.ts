import "./style.css";
import IDBManager from "./js/IDBManager";

// Constants
const cspMeta = document.createElement("meta");
const mainSection = document.createElement("section");
const mainSectionHeading = document.createElement("h1");
const mainSectionSubHeadingText = document.createElement("h2");

// Document Head
cspMeta.httpEquiv = "Content-Security-Policy";
cspMeta.content = "script-src 'self' 'unsafe-inline';";
document.head.appendChild(cspMeta);

// Document Body
document.body.appendChild(mainSection);
mainSection.appendChild(mainSectionHeading);
mainSection.appendChild(mainSectionSubHeadingText);
mainSectionHeading.appendChild(new Text("Capacitron-Pack-Type"));
mainSectionSubHeadingText.appendChild(new Text("> index.ts"));

/**
 * Formats Bytes in a readable manner
 * @param bytes The number to format
 * @param decimals The number of decimals to round to
 * @returns A string containing a number rounded to the number of decimals provided, and
 * the most appropropriate size
 */
function formatBytes(bytes: number, decimals = 2) {
  if (bytes === 0) return "0 Bytes";

  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}

// Check for available storage on the device and show estimated usage
if (navigator && "storage" in navigator) {
  const newParagraph = document.createElement("p");
  navigator.storage.estimate().then((value) =>
    newParagraph.appendChild(
      new Text(
        `→ Storage estimate is \
    ${formatBytes(value.usage)} out of \
    ${formatBytes(value.quota)}`
      )
    )
  );
  mainSection.appendChild(newParagraph);
} else {
  const newParagraph = document.createElement("p");
  newParagraph.appendChild(new Text(`→ Can't estimate storage`));
  mainSection.appendChild(newParagraph);
}

// Check for IndexedDB suppoert
if ("indexedDB" in window) {
  const newParagraph = document.createElement("p");
  newParagraph.appendChild(new Text("→ indexedDB is support by your device"));
  mainSection.appendChild(newParagraph);

  // Instantiate the IndexedDB Manager class
  const idbm = new IDBManager("test", 1);
} else {
  const newParagraph = document.createElement("p");
  newParagraph.appendChild(
    new Text("→ WARNING: indexedDB is NOT support by your device")
  );
  mainSection.appendChild(newParagraph);
}
