export default class IDBManager {
  private db: IDBDatabase;
  readonly name: string;
  readonly version: number;
  readonly idb: IDBFactory;

  /**
   * Registers an IDBDatabase by name and version. Opens that DB as required for transactions.
   * @param name
   * @param version
   */
  constructor(name: string, version: number = 1) {
    this.name = name;
    this.version = version;
    this.idb = window.indexedDB;

    this.init();
  }

  /**
   * Open the IDB and perform necessary initial transactions.
   */
  private async init() {
    this.db = await this.openDB(this.name, this.version);
    console.log(this.db instanceof IDBDatabase && this.db.name === this.name);
  }

  /**
   * Attempt to open and return the IDBDatabase in a Promise wrapper
   * @param name
   * @param version
   * @returns The IDBDatabase
   */
  private openDB(name: string, version: number): Promise<IDBDatabase> {
    return new Promise((resolve, reject) => {
      const openRequest: IDBOpenDBRequest = this.idb.open(name, version);

      openRequest.onsuccess = (e) => resolve(openRequest.result);
      openRequest.onerror = (e) => reject(e);
      openRequest.onupgradeneeded = (e) => {
        // Upgrade the DB.
        console.log("Upgrading the DB");
        this.upgradeDB(openRequest.result, e);
      };
    });
  }

  private upgradeDB(db: IDBDatabase, e: IDBVersionChangeEvent): void {
    // Set up initial stores
    if (e.oldVersion < 1) {
    }
  }

  /**
   * Quickly checks if the IDBDatabase.name property is accessible and matches the name provided to the constructor.
   * @returns True if the IDBDatabase is accessible
   */
  isReady() {
    return this.db instanceof IDBDatabase && this.db.name === this.name;
  }
}
